package ec.consulti.practica.controller;

import ec.consulti.practica.dto.request.UserRQ;
import ec.consulti.practica.entity.UserEntity;
import ec.consulti.practica.service.EmailService;
import ec.consulti.practica.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    //Endpoint para poder iniciar sesion
    @PostMapping("/login")
    public ResponseEntity<UserEntity> loginUser(@RequestBody UserRQ userRQ) {
        try {
            return ResponseEntity.ok(this.userService.loginUser(userRQ));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/sendEmail/{email}")
    public ResponseEntity<String> sendEmail(@PathVariable String email) {
        try {
            userService.sendEmail(email);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
