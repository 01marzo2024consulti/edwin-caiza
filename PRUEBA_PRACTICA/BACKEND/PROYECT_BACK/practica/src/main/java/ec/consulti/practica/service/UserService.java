package ec.consulti.practica.service;

import ec.consulti.practica.dto.request.UserRQ;
import ec.consulti.practica.entity.UserEntity;
import ec.consulti.practica.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final EmailService emailService;

    //Funcion que me permite hacer el login de un usuario
    public UserEntity loginUser(UserRQ userRQ) throws Exception {
        UserEntity user = this.userRepository.findByUsernameAndPassword(userRQ.getUsername(), userRQ.getPassword());
        if (user == null) throw new Exception("User not found");
        return user;
    }

    public String sendEmail(String email) {
        try {
            this.emailService.sendEmail(email, "Email enviado desde Aplicacion Practica", "Email desde practica pantalla productos");
            return "Email enviado";
        } catch (Exception e) {
            return "Error al enviar email";
        }
    }
}
