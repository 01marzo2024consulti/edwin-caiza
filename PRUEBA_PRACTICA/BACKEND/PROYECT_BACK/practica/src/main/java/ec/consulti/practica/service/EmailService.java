package ec.consulti.practica.service;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final JavaMailSender mailSender;

    //Funcion que me permite enviar un correo
    public void  sendEmail(String email,String subject,String body) {
        SimpleMailMessage mensaje=new SimpleMailMessage();
        mensaje.setFrom("efcaiza6@gmail.com");
        mensaje.setTo(email);
        mensaje.setSubject(subject);
        mensaje.setText(body);

        mailSender.send(mensaje);
    }
}
