package ec.consulti.practica.dto.request;

import lombok.Data;

@Data
public class UserRQ {
    private String username;
    private String password;
}
