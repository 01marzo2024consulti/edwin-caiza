package ec.consulti.practica.service;

import ec.consulti.practica.entity.ProductoEntity;
import ec.consulti.practica.entity.UserEntity;
import ec.consulti.practica.repository.ProductoRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ProductoService {
    private final ProductoRepository productoRepository;
    private final EmailService emailService;

    //Funcion que me permite guardar un producto nuevo
    public ProductoEntity save(ProductoEntity producto) throws Exception {
        if (productoRepository.findByNombre(producto.getNombre()) != null) {
            throw new Exception("Ya existe un producto con ese nombre");
        }
        if (producto.getNombre().isEmpty()) {
            producto.setNombre("N/A");
        }
        if (producto.getDescripcion().isEmpty()) {
            producto.setDescripcion("N/A");
        }
        if (producto.getImagen().isEmpty()) {
            producto.setImagen("N/A");
        }
        if (producto.getMarca().isEmpty()) {
            producto.setMarca("N/A");
        }
        if (producto.getValor() == null) {
            producto.setValor(0.0);
        }
        producto.setEstado("Activo");
        producto.setFechaCreacion(LocalDateTime.now());
        return productoRepository.save(producto);
    }

    //Funcion que me permite obtener todos los productos
    public List<ProductoEntity> findAll() {
        return productoRepository.findAll();
    }

    //Funcion que me permite obtener un producto por su ID
    public ProductoEntity findById(Integer id) throws Exception {
        if (productoRepository.findById(id).isEmpty()) {
            throw new Exception("El producto no existe");
        }
        return productoRepository.findById(id).get();
    }

    //Funcion que me permite actualizar la imagen
    public ProductoEntity updateImagen(String imgBase64, Integer id) throws Exception {
        ProductoEntity producto = productoRepository.findById(id).orElse(null);
        if (producto == null) {
            throw new Exception("El producto no existe");
        }
        producto.setImagen(imgBase64);
        return productoRepository.save(producto);
    }

    //Funcion que me permite el borrado logico un producto por su ID
    public void deleteUser(Integer id) {
        ProductoEntity producto = productoRepository.findById(id).orElseThrow(() -> new RuntimeException("Producto no encontrado"));
        producto.setEstado("Inactivo");
        productoRepository.save(producto);
    }
}
