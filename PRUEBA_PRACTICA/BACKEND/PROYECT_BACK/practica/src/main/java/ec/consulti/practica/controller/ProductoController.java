package ec.consulti.practica.controller;

import ec.consulti.practica.entity.ProductoEntity;
import ec.consulti.practica.service.ProductoService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/producto")
@AllArgsConstructor
public class ProductoController {
    private final ProductoService productoService;

    //Endpoint para guardar un nuevo producto en la base de datos
    @PostMapping("/save")
    public ResponseEntity<ProductoEntity> save(@RequestBody ProductoEntity producto) {
        try {
            return ResponseEntity.ok(productoService.save(producto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    //Endpoint para obtener todos los productos activos de la base de datos
    @GetMapping("/allProducts")
    public ResponseEntity<List<ProductoEntity>> findAllByEstadoActivo() {
        try {
            return ResponseEntity.ok(productoService.findAll());
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    //Endpoint para obtener un producto por su ID
    @GetMapping("/{id}")
    public ResponseEntity<ProductoEntity> findById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(productoService.findById(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    //Endpoint para actualizar la imagen
    @PutMapping("/updateImagen/{idProducto}")
    public ResponseEntity<ProductoEntity> updateImagen(@PathVariable Integer idProducto, @RequestBody String imgBase64) {
        try {
            return ResponseEntity.ok(productoService.updateImagen(imgBase64, idProducto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    //Endpoint para borrar un producto por su ID
    @GetMapping("/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Integer id) {
        try {
            productoService.deleteUser(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
