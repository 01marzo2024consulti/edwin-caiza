package ec.consulti.practica.repository;

import ec.consulti.practica.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductoRepository extends JpaRepository<ProductoEntity, Integer> {
    ProductoEntity findByNombre(String nombre);

    List<ProductoEntity> findAllByEstado(String estado);
}
