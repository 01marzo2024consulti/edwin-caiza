# Practica App Angular

Este proyecto es una aplicación desarrollado con Angular versión 17.

## Desplegar app de manera local

### Configuración y Ejecución del Frontend

Para configurar y ejecutar el frontend, sigue estos pasos:

1. Abre una terminal en la carpeta del proyecto frontend.
2. Ejecuta el siguiente comando para instalar las dependencias: 
```cmd
npm install
```

### Ejecución del Frontend

Una vez que hayas instalado las dependencias, levanta el proyecto con el siguiente comando:
```cmd
ng serve
```
Navega a `http://localhost:4200/`

## Desplegar app con docker

### Requisitos Previos

Antes de ejecutar la aplicación, asegúrate de tener instalado lo siguiente:

- Docker

### Cómo Ejecutar la Aplicación con Docker

Sigue estos pasos en el directorio raiz de la aplicación para construir y ejecutar la aplicación en un contenedor Docker:

1. Construye la imagen Docker ejecutando:

```cmd
  docker build -t practica-app .
```

2. Ejecuta la aplicación en un contenedor:

```cmd
docker run -p 4200:80 practica-app
```
3. Navega a `http://localhost:4200/`