import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProductRS } from '../../../models/response/ProductRS';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-details-product',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './details-product.component.html',
  styleUrl: './details-product.component.css',
})
export class DetailsProductComponent {
  product: ProductRS = {};
  constructor(
    public dialogRef: MatDialogRef<DetailsProductComponent>,
    @Inject(MAT_DIALOG_DATA) public productRS: ProductRS
  ) {
    if (this.productRS) {
      this.product = this.productRS;
    } else {
      this.closeModal();
    }
  }

  closeModal() {
    this.dialogRef.close();
  }
}
