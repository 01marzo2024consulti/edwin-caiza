import { Component, inject } from '@angular/core';
import { ProductService } from '../../../services/product.service';
import { ProductRS } from '../../../models/response/ProductRS';
import { MatDialog } from '@angular/material/dialog';
import { CreateProductComponent } from '../create-product/create-product.component';
import { CommonModule } from '@angular/common';
import { DetailsProductComponent } from '../details-product/details-product.component';
import { UpdateImgComponent } from '../update-img/update-img.component';
import { MatIconModule } from '@angular/material/icon';
import { HeaderComponent } from '../../../components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRS } from '../../../models/response/UserRS';
import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'app-list-products',
  standalone: true,
  imports: [CommonModule, MatIconModule, HeaderComponent, FormsModule],
  templateUrl: './list-products.component.html',
  styleUrl: './list-products.component.css',
})
export class ListProductsComponent {
  private productService = inject(ProductService);
  private userService = inject(LoginService);
  products: ProductRS[] = [];
  productsAux: ProductRS[] = [];
  filterByNameProduct: string = '';
  filterByValorProduct?: number = undefined;
  filterData!: String;
  filterByFechaIngresoProduct?: Date = undefined;
  filterByFechaVentaProduct?: Date = undefined;
  userRs: UserRS = {};
  filterByStateProduct: string = '';

  constructor(private dialog: MatDialog) {
    this.userRs = localStorage.getItem('user')
      ? JSON.parse(localStorage.getItem('user')!)
      : {};
    console.log(this.userRs);
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe({
      next: (data) => {
        this.products = data;
        this.products.forEach((product) => {
          const dateArray = product.fechaCreacion?.toString().split(',');
          if (dateArray) {
            product.fechaCreacion = new Date(
              Number(dateArray[0]),
              Number(dateArray[1]) - 1,
              Number(dateArray[2]),
              Number(dateArray[3]),
              Number(dateArray[4])
            );
          }
          const dateArray2 = product.fechaVenta?.toString().split(',');
          if (dateArray2) {
            product.fechaVenta = new Date(
              Number(dateArray2[0]),
              Number(dateArray2[1]) - 1,
              Number(dateArray2[2]),
              0,
              0
            );
          }
        });
        this.productsAux = this.products;
      },
      error: (error) => {},
    });
  }

  saveProduct() {
    const dialogRef = this.dialog.open(CreateProductComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.productService.createProduct(result).subscribe({
          next: (response) => {
            confirm('Producto creado con exito');
            this.getProducts();
          },
          error: (error) => {},
        });
      }
    });
  }

  viewProduct(productRS: ProductRS) {
    const dialogRef = this.dialog.open(DetailsProductComponent, {
      width: '500px',
      data: productRS,
    });
  }

  cargarImg(product: ProductRS) {
    const dialogRef = this.dialog.open(UpdateImgComponent, {
      width: '500px',
      data: product,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.productService.updateImg(result).subscribe({
          next: (response) => {
            confirm('Producto creado con exito');
            this.getProducts();
          },
          error: (error) => {},
        });
      }
    });
  }

  deleteProduct(product: ProductRS) {
    if (product.estado === 'Inactivo') {
      alert('No se puede eliminar un producto Inactivo');
      return;
    }
    const userConfirmed = confirm('¿Desea eliminar el producto?');
    if (userConfirmed) {
      this.productService.deleteUser(product.id!).subscribe({
        next: (response) => {
          alert('Producto eliminado exitosamente');
          this.getProducts();
        },
        error: (error) => {},
      });
    } else {
    }
  }

  applyFilters() {
    this.products = this.productsAux;

    if (this.filterByNameProduct !== '') {
      this.products = this.products.filter((product) => {
        return product.nombre?.includes(this.filterByNameProduct);
      });
    }

    if (this.filterByValorProduct !== undefined) {
      this.products = this.products.filter((product) => {
        return product.valor! > this.filterByValorProduct!;
      });
    }

    if (this.filterByFechaIngresoProduct !== undefined) {
      this.products = this.products.filter((product) => {
        const fechaCreacion = new Date(product.fechaCreacion!);
        const filterDate = new Date(this.filterByFechaIngresoProduct!);
        return fechaCreacion >= filterDate;
      });
    }
    if (this.filterByFechaVentaProduct !== undefined) {
      this.products = this.products.filter((product) => {
        const fechaVenta = new Date(product.fechaVenta!);
        const filterDate = new Date(this.filterByFechaVentaProduct!);
        return fechaVenta >= filterDate;
      });
    }

    if (this.filterByStateProduct !== '') {
      this.products = this.products.filter((product) => {
        return product.estado === this.filterByStateProduct;
      });
    }
  }

  sendEmail() {
    this.userService.sendEmail(this.userRs.email!).subscribe({
      next: (data) => {
        confirm('Email enviado con exito');
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
