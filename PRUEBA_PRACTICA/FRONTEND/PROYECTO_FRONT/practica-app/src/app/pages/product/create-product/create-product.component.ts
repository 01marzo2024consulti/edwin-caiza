import { Component, Inject, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProductRS } from '../../../models/response/ProductRS';
import { ProductService } from '../../../services/product.service';
import { CommonModule } from '@angular/common';
import { ProductRQ } from '../../../models/request/ProductRQ';

@Component({
  selector: 'app-create-product',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './create-product.component.html',
  styleUrl: './create-product.component.css'
})
export class CreateProductComponent {
  myForm: FormGroup;
  private productService = inject(ProductService);
  product: ProductRQ = {};
  imgBase64: string = '';

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateProductComponent>,
    @Inject(MAT_DIALOG_DATA) public productRQ: ProductRQ
  ) {
    this.myForm = this.buildForm();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      nombre: [''],
      descripcion: ['', ],
      valor: ['', [Validators.required, Validators.min(0)]],
      marca: [''],
      fechaVenta: [''],
      imagen: [''],
    });
  }

  submitForm() {
      this.product = this.myForm.value;
      this.product.imagen = this.imgBase64;
      this.dialogRef.close(this.product);
      console.log(this.product);
  }

  closeModal() {
    this.dialogRef.close();
  }

  limpiarForm() {
    this.myForm.reset();
  }

  onFileChange(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();


    reader.onloadend = () => {
      const base64String = reader.result?.toString().split(',')[1] || '';
      this.imgBase64 = base64String;
    }

    if (file) {
      reader.readAsDataURL(file);
    }
  }
}
