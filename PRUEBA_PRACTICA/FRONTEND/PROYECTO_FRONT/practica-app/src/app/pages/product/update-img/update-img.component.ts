import { Component, Inject } from '@angular/core';
import { ProductRS } from '../../../models/response/ProductRS';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-update-img',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './update-img.component.html',
  styleUrl: './update-img.component.css'
})
export class UpdateImgComponent {

  product: ProductRS = {};
  imgBase64: string = "";
  myForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<UpdateImgComponent>,
    @Inject(MAT_DIALOG_DATA) public productRS: ProductRS
  ) {
    if (this.productRS) {
      this.product = this.productRS;
    } else {
      this.closeModal();
    }
    this.myForm = this.buildForm();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      imagen: [''],
    });
  }

  submitForm() {
    if (!this.imgBase64) {
      alert('Debe subir una imagen');
      return
    }
    this.product.imagen = this.imgBase64;
    this.dialogRef.close(this.product);
  }

  closeModal() {
    this.dialogRef.close();
  }

  onFileChange(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();


    reader.onloadend = () => {
      const base64String = reader.result?.toString().split(',')[1] || '';
      this.imgBase64 = base64String;
    }

    if (file) {
      reader.readAsDataURL(file);
    }
  }
}
