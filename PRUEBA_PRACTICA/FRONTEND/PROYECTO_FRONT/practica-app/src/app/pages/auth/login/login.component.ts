import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { UserRQ } from '../../../models/request/UserRQ';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  private loginService = inject(LoginService);
  myForm: FormGroup;
  userRQ: UserRQ = {};
  flag: boolean = true;

  constructor(private fb: FormBuilder, private router: Router) {
    this.myForm = this.buildForm();
  }

  private buildForm(): FormGroup {
    return this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  consult() {
    if (this.myForm.valid) {
      this.userRQ = this.myForm.value;
      this.loginService.login(this.userRQ).subscribe({
        next: (data) => {
          localStorage.setItem('user', JSON.stringify(data));
          this.router.navigate(['/product']);
        },
        error: (err) => {
          this.flag = false;
        },
        complete: () => {},
      });
    } else {
      this.myForm.markAllAsTouched();
    }
  }

  onInput() {
    if (!this.flag) this.flag = true;
  }
}
