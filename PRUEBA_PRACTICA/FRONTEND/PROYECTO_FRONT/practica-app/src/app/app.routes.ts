import { Routes } from '@angular/router';
import { ListProductsComponent } from './pages/product/list-products/list-products.component';
import { LoginComponent } from './pages/auth/login/login.component';

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'product',
    component: ListProductsComponent,
  },
];
