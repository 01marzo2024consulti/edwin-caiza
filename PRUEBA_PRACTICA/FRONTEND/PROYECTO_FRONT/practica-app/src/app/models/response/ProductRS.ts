export interface ProductRS {
  id?: number;
  nombre?: string;
  descripcion?: string;
  valor?: number;
  marca?: string;
  fechaCreacion?: Date;
  fechaVenta?: Date;
  imagen?: string;
  estado?: string;
}
