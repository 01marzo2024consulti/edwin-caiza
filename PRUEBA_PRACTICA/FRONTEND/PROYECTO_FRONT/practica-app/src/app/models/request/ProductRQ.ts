export interface ProductRQ {
  id?: number;
  nombre?: string;
  descripcion?: string;
  valor?: number;
  marca?: string;
  imagen?: string;
  fechaVenta?: Date;
}
