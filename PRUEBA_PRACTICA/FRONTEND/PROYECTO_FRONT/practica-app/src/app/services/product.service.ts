import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { ProductRS } from '../models/response/ProductRS';
import { ProductRQ } from '../models/request/ProductRQ';

const URL = 'http://localhost:8080';
const PRODUCT = URL + '/api/v1/producto';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private http = inject(HttpClient);
  constructor() {}

  getProducts() {
    return this.http.get<ProductRS[]>(`${PRODUCT}/allProducts`);
  }

  createProduct(product: ProductRQ) {
    return this.http.post<ProductRQ>(`${PRODUCT}/save`, product);
  }

  updateImg(product: ProductRQ) {
    return this.http.put<ProductRQ>(
      `${PRODUCT}/updateImagen/${product.id}`,
      product.imagen
    );
  }

  deleteUser(userId: number) {
    return this.http.get<String>(`${PRODUCT}/delete/${userId}`);
  }
}
