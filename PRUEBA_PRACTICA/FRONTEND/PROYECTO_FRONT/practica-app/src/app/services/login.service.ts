import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { UserRQ } from '../models/request/UserRQ';
import { Observable } from 'rxjs';

const URL = 'http://localhost:8080';
const USER = URL + '/api/v1/user';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private http = inject(HttpClient);
  constructor() {}

  login(userRQ: UserRQ) {
    return this.http.post<UserRQ>(`${USER}/login`, userRQ);
  }

  sendEmail(email: string) {
    return this.http.get<string>(`${USER}/sendEmail/${email}`);
  }
}
