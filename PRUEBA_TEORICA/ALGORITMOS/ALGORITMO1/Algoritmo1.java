import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Algoritmo1 {
    public static void main(String[] args) throws Exception {
        placa();
    }

    public static void placa() throws Exception {
        String provincia = "Pichincha";
        String tipo = "Vehículos comerciales";
        int bloque = 1;
        if (provincia(provincia) == "" || tipo(tipo) == "" || bloque <= 0) {
            throw new Exception("Porfavor ingrese bien los datos");
        }
        System.out.println(stringRandom(provincia, tipo, bloque));
    }

    public static String stringRandom(String provincia, String tipo, Integer bloque) {
        String placa = "";
        int i = 0;
        while (i < bloque) {
            int j = 0;
            while (j < 3) {
                placa += provincia(provincia) + tipo(tipo) + numberRandom() + "-" + numberRandom2() + "\n";
                j++;
            }
            i++;
            placa += "\n";
        }
        return placa;
    }

    // Asigna una letra dependiendo de la Provincia
    public static String provincia(String provincia) {
        Map<String, String> map = new HashMap<>();
        map.put("Guayas", "G");
        map.put("Pichincha", "P");
        map.put("Santa Elena", "Y");
        map.put("Orellana", "Q");
        map.put("Carchi", "C");
        if (map.containsKey(provincia)) {
            return map.get(provincia);
        } else {
            return "";

        }
    }

    // Asigna una letra dependiendo del tipo
    public static String tipo(String tipo) {

        Map<String, List<String>> map = new HashMap<>();

        List<String> vehiculosComerciales = Arrays.asList("A", "U", "Z");
        map.put("Vehículos comerciales", vehiculosComerciales);
        List<String> vehiculosGubernamentales = Arrays.asList("E");
        map.put("Vehículos gubernamentales", vehiculosGubernamentales);
        List<String> vehiculosUsoOficial = Arrays.asList("X");
        map.put("Vehículos de uso oficial", vehiculosUsoOficial);
        List<String> vehoculosParticular = Arrays.asList("B", "C", "D", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "O", "P", "R", "S", "T", "V", "W", "X", "Y");
        map.put("Vehículo particular", vehoculosParticular);
        if (map.containsKey(tipo)) {
            List<String> lista = map.get(tipo);
            if (!lista.isEmpty()) {
                int indiceAleatorio = new Random().nextInt(lista.size());
                return lista.get(indiceAleatorio);
            }
        }
        return "";
    }

    public static String numberRandom() {
        Random random = new Random();
        return String.valueOf(random.nextInt(10));
    }

    // Funcion que permite crear los digitos 5 al 8 aleatoriamente
    public static String numberRandom2() {
        Random random = new Random();
        String digits = "";
        int i = 0;
        while (i < 4) {
            digits += String.valueOf((random.nextInt(7) + 3));
            i++;
        }
        return digits;
    }
}