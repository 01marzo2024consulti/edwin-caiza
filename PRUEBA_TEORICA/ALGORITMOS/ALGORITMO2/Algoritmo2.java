package ALGORITMOS.ALGORITMO2;

public class Algoritmo2 {

    public static void main(String[] args) throws Exception {
        newWord();
    }

    public static void newWord() throws Exception {
        // String word = "playaSalinas";
        String word = "casarotas";
        
        if (word.length() % 2 != 0) {
            throw new Exception("La cadena es impar con longitud " + word.length());
        } else {
            System.out.println(turnWord(word));
        }
    }

    //Funcion que invierte la cadena
    public static String turnWord(String word) {
        String string1 = word.substring(0, word.length() / 2);

        String string2 = word.substring(word.length() / 2, word.length());

        return string2 + string1;
    }
}
